\c tensor
CREATE TABLE departments (
	id serial PRIMARY KEY,
	parent_id integer,
	name varchar,
	type integer
);
CREATE INDEX parent_id_index ON departments (parent_id);
