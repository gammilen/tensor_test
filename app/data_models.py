import psycopg2 as pc
import psycopg2.sql as sql
import json
import config



class DBModel(object):
	"""
	A class to interact with database
	...

	Attributes:
	-----------
	_connection: psycopg.connection
		db connection
	_cursor: psycopg.cursor
		db cursor
	"""
	def __init__(self, *args, **kwargs):
		self._connection = self._make_connection(**kwargs)
		self._cursor = self._make_cursor(self.connection)

	@property
	def connection(self):
		"""Return connection if it is open or create it"""
		if self._connection.closed != 0:
			self.reconnect()	
		return self._connection

	@property
	def cursor(self):
		"""Return cursor if it is open or create it"""
		if self._cursor.closed:
			self._cursor = self._make_cursor(self.connection)
		return self._cursor
			
	@staticmethod
	def _make_connection(dbname, user, host, password):
		return pc.connect(
			dbname=dbname, user=user, host=host, password=password)

	@staticmethod
	def _make_cursor(connection):
		return connection.cursor()

	@staticmethod
	def form_query_with_table_name(query, t_name):
		"""Insert table name into query
		Query needs '{}' instead of table name"""
		return sql.SQL(query).format(sql.Identifier(t_name))

	def reconnect(self, number=5):
		"""Try to make connection with old connection's parameters
		`number` is a number of attempts
		"""
		for _ in range(number):
			try:
				self._connection = self._make_connection(
					self._connection.get_dsn_parameters())
			except pc.Error:
				pass
			else:
				return
		raise ConnectionError(
			"Невозможно восстановить соединение")

	def execute_query_in_table_with_params(
			self, raw_query, table_name, parameters=None, 
			many=False, attempts=1):
		"""Prepare query with table_name
		Try execute query with given parameters `attempts` times
		"""
		query = self.form_query_with_table_name(
			raw_query, table_name)
		for _ in range(attempts):
			try:
				result = self.execute(query, parameters, many)
			except pc.Error as e:
				pass
			else:
				return result
		else:
			raise ConnectionError(
			"Невозможно выполнить запрос")

	def execute(self, query, parameters, many):
		"""Execute query with cursor
		Return result of execution
		`many` - more than one set of parameters in `parameters`
		"""
		cur = self.cursor
		if many:
			cur.executemany(query, parameters)
		else:
			cur.execute(query, parameters)
		return cur.fetchall() if cur.description else None
		
	def commit(self):
		try:
			self._connection.commit()
		except pc.Error:
			raise ConnectionError(
				"Не удалось завершить транзакцию")

	def rollback(self):
		if self._connection.closed == 0:
			self._connection.rollback()

	def close_connection(self):
		self._cursor.close()
		self._connection.close()


class DepartmentsModel():
	"""
	A class used to interact with table 
	containing information about officies, departments and employees in it
	...

	Attributes
	----------
	t_name : str
		the name of table in database
	employee_type: int
		the value of type column which match employees rows
	office_type: int
		the value of type column which match office rows
	db_model: DBModel
		the model of database to interact with it
	"""
	t_name = 'departments'
	employee_type = 3
	office_type = 1	

	def __init__(self, db_model, *args, **kwargs):
		self.db_model = db_model

	def end_session(self):
		self.db_model.close_connection()

	def confirm_transactions(self):
		self.db_model.commit()

	def cancel_transactions(self):
		self.db_model.rollback()

	def execute_query_with_params(
			self, query, parameters=None, many=False):
		"""Delegate to the database model execution of query"""
		result =  self.db_model.execute_query_in_table_with_params(
			query, self.t_name, parameters, many)
		return result

	def is_empty(self):
		"""Send request to get data in the table
		Return True if result is empty"""
		query = "SELECT * FROM {} LIMIT 1;"
		result = self.execute_query_with_params(query)
		return result == [] 

	def delete_all(self, confirm=True):
		"""Send request to delete all rows in the table"""
		query = "DELETE FROM {};"
		self.execute_query_with_params(query)
		

	def insert_many(self, values, confirm=True):
		"""Send request of inserting multiple sets of data in the table"""
		query = """ INSERT INTO {} (id, parent_id, name, type) 
			VALUES (%(id)s, %(ParentId)s, %(Name)s, %(Type)s);
			"""
		self.execute_query_with_params(query, values, many=True)
		

	def find_employee_office_name_and_id(self, employee_id):
		"""Send request of hieracial search to find employee's office 
		through unknown number of departments"""
		query = """ WITH RECURSIVE office AS (
			SELECT id, parent_id, name, type
			FROM {0} WHERE id = %(id)s and type = %(emp_type)s
			UNION
				SELECT t.id, t.parent_id, t.name, t.type
				FROM {0} as t
				INNER JOIN office o ON t.id=o.parent_id
		) SELECT id, name FROM office WHERE type = %(office_type)s;
		"""
		params = {"id": employee_id, "emp_type": self.employee_type, 
			"office_type": self.office_type}
		result = self.execute_query_with_params(query, params)
		return result[0] if result else None

	def find_office_employees(self, office_id):
		"""Send request of hieracial search to find office employees 
		through unknown number of departments"""
		query = """	WITH RECURSIVE employees AS (
			SELECT id, parent_id, name, type
			FROM {0} WHERE id = %(id)s and type = %(office_type)s
			UNION
				SELECT t.id, t.parent_id, t.name, t.type
				FROM {0} as t
				INNER JOIN employees e ON e.id=t.parent_id
		) SELECT name FROM employees WHERE type = %(emp_type)s;
		"""
		params = {"id": office_id, "emp_type": self.employee_type,
			"office_type": self.office_type}
		result = self.execute_query_with_params(query, params)
		return [emp[0] for emp in result]

	def is_employee(self, emp_id):
		"""Send request to get record about emloyee with given id
		Return True if result is not empty"""
		query = "SELECT * FROM {} WHERE id = %(id)s AND type = %(type)s;"
		params = {"id": emp_id, "type": self.employee_type}
		result = self.execute_query_with_params(query, params)
		return result != []




