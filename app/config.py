import os

DATABASE_PARAMS = {
	'dbname': os.environ.get('POSTGRES_DB'),
	'user': os.environ.get('POSTGRES_USER'),
	'host': 'db',
	'password': os.environ.get('POSTGRES_PASSWORD'),
}
# A file name to get data about offices, its departments and its employees
DATA_FILE = 'data.json'