import sys
from interface import UserInterface
			

def main(*args, **kwargs):
	try:
		interface = UserInterface()
		interface.start()
	except KeyboardInterrupt:
		return 1
	finally:
		try:
			interface.close()
		except NameError:
			pass
	return 0


if __name__ == "__main__":
	sys.exit(main(sys.argv))


