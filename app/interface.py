from data_models import DepartmentsModel, DBModel
import config
import commands as c

class UserInterface():
	"""
	A class used to execute user's commands 
	...

	Attributes:
	-----------
	is_active: bool
		represents whether interface can proceed user's input or not
	dep_model: DepartmentModel
		model for commands to interact with departments table
	options: dict
		`command_name`: Command
		commands with their names, functions to run, 
		services parameters and description

	"""
	def __init__(self):
		self.is_active = True
		self.dep_model = self._create_dep_model()
		options = [
			c.ImportCommand(self.dep_model),
			c.GetEmployeesCommand(self.dep_model),
			c.Command("exit", "Завершает программу", self.exit_command),
			c.Command("help", "Выводит данное меню", self.help_command),
			]
		self.options = {o.name: o for o in options}

	def _option_exists(self, command):
		"""Check command name in available options"""
		return command in self.options.keys()

	def _get_option(self, command):
		"""Return command for interaction"""
		return self.options[command]

	def _get_service_parameters(self, command):
		"""Return required service parameters 
		for command to run by command name
		"""
		return self.options[command]["service_parameters"]

	def _create_dep_model(self):
		"""Create new department model with required database model"""
		db = DBModel(**config.DATABASE_PARAMS)
		return DepartmentsModel(db)

	def exit_command(self):
		"""Command 
		Make interface unavailable to process user's commands
		"""
		self.is_active = False
		return "Программа будет завершена"

	def help_command(self):
		"""Command
		Return information about all available options
		"""
		response = "Список доступных команд:\n" \
			+ "\n\n".join(
				[o.name+":\n"+o.description \
				for o in self.options.values()])
		
		return response


	def close(self):
		"""Inform components (department model)
		that the session has ended"""
		self.dep_model.end_session()
	
	def split_command_and_parameters(self, input_command):
		"""Split command string to command word and parameters"""
		input_list = input_command.split(" ")
		return input_list[0], tuple(input_list[1:])	

	def validate_command(self, command):
		"""Check if the command is correct:
			- existing
		"""
		errors = []
		if not self._option_exists(command):
			errors.append("Такой опции не существует")
		return errors

	def run_command(self, command, parameters):
		"""Execute command run function with all 
		(service and given) parameters and 
		Return message about result
		"""
		try:
			com = self._get_option(command)
			com.load_parameters(parameters)
			return com.run()
		except c.ArgumentError as e:
			return "Неправильная команда:\n"+str(e)
		except TypeError:
			return "Неправильная команда"
		except ConnectionError as e:
			return "Ошибка взаимодействия с базой данных:\n"+str(e)
		except ValueError as e:
			return "Неверное значение параметра:\n"+str(e)

	def process_command(self, input_command):
		"""Define the command and its parameters
		If command was right, run it with this parameters
		Shows command result
		"""
		command, parameters = self.split_command_and_parameters(input_command)
		errors = self.validate_command(command)
		if not errors:
			response = self.run_command(command, parameters)
			print(response)
		else:
			print("\n".join(errors))

	def get_user_command(self):
		"""A way, which programm gets command text
		"""
		return input("Введите команду: ")

	def start(self):
		"""Wait for command and execute it"""
		while self.is_active:
			option = self.get_user_command()
			self.process_command(option)