import config
import json


class ArgumentError(BaseException):
	"""Exception raise when wrong argument is passed in function"""
	pass


class Command():
	"""
	A class with information about 
	command and functions to run it
	...

	Attributes:
	-----------
	name: str
		a command name
	description: str
		what command do
	command_function: function
		function to run
	parameters: tuple
		parameters to pass in command_function
	"""
	def __init__(self, name, desc, function):
		self.name = name
		self.description = desc
		self.command_function = function
		self.parameters = tuple()
	
	def load_parameters(self, params):
		"""Process parameters to command"""
		self.parameters = params

	def run(self):
		"""Execute command function with cimmand parameters"""
		return self.command_function(*self.parameters)


class ImportCommand(Command):
	def __init__(self, dep_model):
		self.dep = dep_model
		name = "import"
		desc = "Импортирует данные из " \
			"json `DATA_FILE` в таблицу `DEPARTMENTS_TABLE_NAME`"
		func = self.import_data
		super(ImportCommand, self).__init__(name, desc, func)

	@staticmethod
	def _get_json_data(f_name):
		"""Load data from json file to dictionary
		Raise exception if not successful"""
		try:
			with open(f_name, "r") as j_file:
				return json.load(j_file)
		except FileNotFoundError:
			raise ValueError("Файла {} не существует".format(f_name))
		except:
			raise ValueError("Не удалось получить \
				данные из файла {}".format(f_name))

	def import_data(self):
		"""Import data from file to empty department table"""
		try:
			if not self.dep.is_empty():
				self.dep.delete_all()
			data = self._get_json_data(config.DATA_FILE)
			self.dep.insert_many(data)
		except:
			self.dep.cancel_transactions()
			raise
		else:
			self.dep.confirm_transactions()
			return "Данные успешно импортированы"


class GetEmployeesCommand(Command):
	def __init__(self, dep_model):
		self.dep = dep_model
		name = "get_employees"
		desc = "Необходимые параметры:\n" \
			"employee_id - id сотрудника\n" \
			"Отображает информацию о сотрудниках офиса, " \
			"которому принадлежит сотрудник с указанным id"
		func = self.get_employees
		super(GetEmployeesCommand, self).__init__(name, desc, func)

	def get_employees(self, emp_id=None):
		"""Find employees from office 
		in which employee with `emp_id` work
		"""
		self.validate_emp_id(emp_id)
		try:
			if self.dep.is_employee(emp_id):
				office = self.dep. \
				find_employee_office_name_and_id(emp_id)
				if office:
					office_id, office_name = office
					employees = self.dep.find_office_employees(office_id)
					result = "Сотрудники офиса '" \
						+ office_name \
						+ "': "+", ".join(employees)
				else:
					result = "У данного сотрудника не найден офис"
			else:
				result = "Сотрудника с данным id не существует"
		except:
			self.dep.cancel_transactions()
			raise
		else:
			self.dep.confirm_transactions()
			return result

	def validate_emp_id(self, emp_id):
		"""Check if the employee id is correct:
			- existing
			- integer
		"""
		if not emp_id or not emp_id.isnumeric():
			raise ArgumentError(
				"Неверное значение параметра id сотрудника")
